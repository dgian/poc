let path = require('path');
let HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: __dirname + '/src',
    entry: {
        app: './app'
    },
    output: {
        path: path.resolve(__dirname, 'bin'),
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.js', '.css']
    },
    module: {
        loaders: [
            {
                test: /\.[tj]s$/,
                loaders: ['awesome-typescript-loader']
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'src', 'index.html')
        })
    ]
};