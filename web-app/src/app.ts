import * as angular from 'angular';
import * as uirouter from 'angular-ui-router';
import {MainRoutes} from './routes';
import {module_order} from "./order/module.order";
import {module_customer} from "./customer/module.customer";
import './app.css';

export const module_app:string = 'Module.App';

export class App {

    app:angular.IModule;

    constructor() {
        this.app = angular.module(module_app, [
            uirouter,
            module_customer,
            module_order
        ]);
        MainRoutes.addRoutes(this.app);
    }
}

new App();