import {Customer} from "./model/Customer";
import {CustomerService} from "./services/CustomerService";

export class CustomerController {
    customer: Customer;

    /*@ngInject*/
    constructor(private $scope:any,
                private uuid:string,
                private customerService: CustomerService) {
        if (uuid) {
            customerService
                .read(uuid)
                .then((customer) => {
                    this.$scope.$broadcast('load-customer', customer.data);
                });
        }

        $scope.customer = this.customer;
    }
}