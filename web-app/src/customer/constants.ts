import {Endpoint} from "../constants";

export class CustomerEndpoint extends Endpoint{
    static CREATE = Endpoint.BASE_URL + "/customer";
    static READ   = (uuid: string) => Endpoint.BASE_URL + "/customer/" + uuid;
    static UPDATE = Endpoint.BASE_URL + "/customer";
    static LIST   = Endpoint.BASE_URL + "/customer";
}