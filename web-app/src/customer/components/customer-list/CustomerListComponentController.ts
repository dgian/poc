import {Customer} from "../../model/Customer";
import {CustomerService} from "../../services/CustomerService";
import * as angular from "angular";

export class CustomerListComponentController {
    /*@ngInject*/
    constructor(private $rootScope: any,
                private $scope: any,
                private customerService: CustomerService) {
        this.$scope.customers = [];
    }

    public $onInit = () => {
        this.customerService
            .list()
            .then((response: angular.IResponse<Customer[]>) => {
                for (let customer of response.data) {
                    this.appendCustomer(customer);
                }
            });

        this.$rootScope.$on('create-customer', (evt, customer: Customer) => {
            this.appendCustomer(customer)
        });
    };

    public appendCustomer = (customer: Customer) => {
        this.$scope.customers.push(customer);
    }
}