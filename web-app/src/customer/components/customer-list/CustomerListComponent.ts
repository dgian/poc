import {Component} from "../Component";
import {CustomerListComponentController} from "./CustomerListComponentController";

export class CustomerListComponent extends Component {
    constructor() {
        super('./src/customer/components/customer-list/customer-list.tpl.html', CustomerListComponentController);
    }
}