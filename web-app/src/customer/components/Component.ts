import * as angular from 'angular';

export class Component {
    public bindings?: {[boundProperty: string]: string};

    public transclude?: boolean | {[slot: string]: string};

    public require?: {[controller: string]: string};

    constructor(public templateUrl: string | angular.Injectable<(...args: any[]) => string>,
                public controller: string | angular.Injectable<angular.IControllerConstructor>) {

    }
}