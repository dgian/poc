import {CustomerService} from "../../services/CustomerService";
import {Customer} from "../../model/Customer";
import * as angular from "angular";

export class CustomerFormComponentController {
    action: string;
    showFeedback: boolean;

    /*@ngInject*/
    constructor(private $scope:any,
                private customerService: CustomerService) {
        this.$scope.customer = new Customer();
    }

    public $onInit = () => {
        this.$scope.$on('load-customer', (evt, customer: Customer) => {
            this.$scope.customer = customer;
        });
    };


    save() {
        if (this.action == 'Update') {
            return this.customerService
                .update(this.$scope.customer)
                .then((response: angular.IResponse<Customer>) => {
                    this.$scope.$emit('update-customer', response.data);
                    this.$scope.showFeedback = true;
                    this.showFeedback = true;
                })
        } else {
            return this.customerService
                .create(this.$scope.customer)
                .then((response: angular.IResponse<Customer>) => {
                    this.$scope.$emit('create-customer', response.data);
                });

        }

    }
}