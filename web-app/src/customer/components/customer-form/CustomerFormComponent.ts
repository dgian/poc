import {Component} from "../Component";
import {CustomerFormComponentController} from "./CustomerFormComponentController";

export class CustomerFormComponent extends Component{
    constructor() {
        super('./src/customer/components/customer-form/customer-form.tpl.html', CustomerFormComponentController);
        this.bindings = {
            title: '<',
            action: '<',
            customer: '='
        }
    }
}