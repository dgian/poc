import * as angular from 'angular';
import {CustomerFormComponent} from './components/customer-form/CustomerFormComponent';
import {CustomerService} from "./services/CustomerService";
import {CustomerListComponent} from "./components/customer-list/CustomerListComponent";
import {module_order} from "../order/module.order";

export const module_customer:string = 'Module.Customer';

angular.module(module_customer, [module_order])
       .component('customerForm', new CustomerFormComponent())
       .component('customerList', new CustomerListComponent())
       .service('customerService', CustomerService);