import {Customer} from "../model/Customer";
import {CustomerEndpoint} from "../constants";
import * as angular from "angular";

export class CustomerService {

    /*@ngInject*/
    constructor(private $http: angular.IHTTPService) {
    }

    create(customer: Customer): angular.IPromise<Customer> {
        return this.$http
            .post(CustomerEndpoint.CREATE, customer)
            .then((response: angular.IResponse<Customer>) => response);
    }

    list(): angular.IPromise<Customer[]> {
        return this.$http
            .get(CustomerEndpoint.LIST)
            .then((response: angular.IResponse<Customer[]>) => response);
    }

    read(uuid: string): angular.IPromise<Customer> {
        return this.$http
            .get(CustomerEndpoint.READ(uuid))
            .then((response: angular.IResponse<Customer>) => response);
    }

    update(customer: Customer): angular.IPromise<Customer> {
        return this.$http
            .put(CustomerEndpoint.UPDATE, customer)
            .then((response: angular.IResponse<Customer>) => response);
    }
}