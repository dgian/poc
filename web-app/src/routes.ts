import * as angular from "angular";
import {CustomerController} from "./customer/CustomerController";

export class MainRoutes {
    public static addRoutes(app: angular.IModule) {
        app.config(["$stateProvider", ($stateProvider: angular.ui.IStateProvider) => {
            $stateProvider
                .state("customer", {
                    url: "/customer",
                    views: {
                        "main": {
                            templateUrl: "./src/customer/customer.tpl.html"
                        }
                    },
                    resolve: {
                        uuid: ['$stateParams', ($stateParams)=> {
                            return $stateParams.uuid;
                        }]
                    }
                })
                .state("customer_view", {
                    url: "/{uuid}/view",
                    views: {
                        "main": {
                            templateUrl: "./src/customer/customer_view.tpl.html",
                            controller: CustomerController
                        }
                    },
                    resolve: {
                        uuid: ['$stateParams', ($stateParams)=> {
                            return $stateParams.uuid;
                        }]
                    }
                });
        }]);

        app.config(["$urlRouterProvider",($urlRouterProvider:angular.ui.IUrlRouterProvider) => {
            $urlRouterProvider.otherwise(($injector)=> {
                let $state = $injector.get("$state");
                $state.go("customer");
            });
        }]);
    }
}