import {Endpoint} from "../constants";

export class OrderEndpoint extends Endpoint {
    static CREATE      = Endpoint.BASE_URL + "/order";
    static BY_CUSTOMER = (uuid: string) => Endpoint.BASE_URL + "/customer/" + uuid + "/orders";
    static DELETE      = (uuid: string) => Endpoint.BASE_URL + "/order/" + uuid;
}