export class Order {
    id: string;
    customerId: string;
    createdAt: string;
    info: string;
}