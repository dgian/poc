import {OrderService} from "../../services/OrderService";
import {Order} from "../../model/Order";
import {Customer} from "../../../customer/model/Customer";
import * as angular from "angular";

export class OrderFormComponentController {
    constructor(private $scope:any,
                private orderService: OrderService) {
        this.$scope.order = new Order();
        this.$scope.order.customerId = this.$scope.$parent.uuid;
    }

    public $onInit = () => {
        this.$scope.$on('load-customer', (evt, customer: Customer) => {
            this.$scope.order.customerId = customer.id;
        });
    };

    create() {
        console.log("order", this.$scope.order);
        return this.orderService
            .create(this.$scope.order)
            .then((response: angular.IResponse<Order>) => {
                this.$scope.$emit('create-order', response.data);
            });
    }
}