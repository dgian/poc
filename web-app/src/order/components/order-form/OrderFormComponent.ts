import {Component} from "../../../customer/components/Component";
import {OrderFormComponentController} from "./OrderFormComponentController";

export class OrderFormComponent extends Component {
    constructor() {
        super('./src/order/components/order-form/order-form.tpl.html', OrderFormComponentController);
        this.bindings = {
            action: '<'
        }
    }
}