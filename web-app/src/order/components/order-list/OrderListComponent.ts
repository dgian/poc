import {Component} from "../../../customer/components/Component";
import {OrderListComponentController} from "./OrderListComponentController";

export class OrderListComponent extends Component {
    constructor() {
        super('./src/order/components/order-list/order-list.tpl.html', OrderListComponentController);
    }
}