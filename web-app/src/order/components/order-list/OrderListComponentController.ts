import {OrderService} from "../../services/OrderService";
import {Order} from "../../model/Order";
import {Customer} from "../../../customer/model/Customer";
import * as angular from "angular";

export class OrderListComponentController {
    /*@ngInject*/
    constructor(private $rootScope: any,
                private $scope: any,
                private orderService: OrderService) {
        this.$scope.orders = [];
    }

    public $onInit = () => {
        this.$scope.$on('load-customer', (evt, customer: Customer) => {
            this.orderService
                .filterByCustomer(customer.id)
                .then((response: angular.IResponse<Order[]>) => {
                    for (let order of response.data) {
                        this.appendOrder(order);
                    }
                });

        });

        this.$rootScope.$on('create-order', (evt, order: Order) => {
            this.appendOrder(order);
        });
    };

    public delete(order: Order) {
        this.orderService.delete(order).then((response: angular.IResponse<Order>) => {
            this.$scope.orders.splice(
                this.$scope.orders.indexOf(order),
                1
            );
        });
    }

    public appendOrder = (order: Order) => {
        this.$scope.orders.push(order);
    }
}