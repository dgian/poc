import * as angular from 'angular';
import {OrderListComponent} from "./components/order-list/OrderListComponent";
import {OrderService} from "./services/OrderService";
import {OrderFormComponent} from "./components/order-form/OrderFormComponent";

export const module_order:string = 'Module.Order';

angular.module(module_order, [])
    .component('orderList', new OrderListComponent())
    .component('orderForm', new OrderFormComponent())
    .service('orderService', OrderService);