import {Order} from "../model/Order";
import {OrderEndpoint} from "../constants";
import * as angular from "angular";

export class OrderService {
    /*@ngInject*/
    constructor(private $http: angular.IHTTPService) {
    }

    filterByCustomer(cstUuid: string): angular.IPromise<Order[]> {
        return this.$http
            .get(OrderEndpoint.BY_CUSTOMER(cstUuid))
            .then((response: angular.IResponse<Order[]>) => response);
    }

    create(order: Order): angular.IPromise<Order> {
        return this.$http
            .post(OrderEndpoint.CREATE, order)
            .then((response: angular.IResponse<Order>) => response);
    }

    delete(order: Order): angular.IPromise<Order> {
        return this.$http
            .delete(OrderEndpoint.DELETE(order.id))
            .then((response: angular.IResponse<Order>) => response);
    }
}