## Assumptions
This guide assumes that there is a local installation of `npm`, `nodejs`, `mongo` and `maven` on the target system.

## Run back-end
To build and run the back-end server, do the following:

```bash
$ mvn package
$ java -jar target/poc-mongo-spring-0.0.1-SNAPSHOT.jar
```

## Run front-end
To build and run the front-end server, do the following:


```bash
$ cd web-app
$ npm start
```
----

You can access http://localhost:8081 with any browser and you should see the toy application doing its magic :)