package net.amdtelecom.poc;

import net.amdtelecom.poc.customers.data.CustomerRepository;
import net.amdtelecom.poc.customers.service.CustomerMapperConfig;
import net.amdtelecom.poc.customers.service.CustomerService;
import net.amdtelecom.poc.orders.data.OrderRepository;
import net.amdtelecom.poc.orders.service.OrderMapperConfig;
import net.amdtelecom.poc.orders.service.OrderService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = {CustomerRepository.class, OrderRepository.class})
@Import(value = {OrderMapperConfig.class, CustomerMapperConfig.class})
public class PocEntryPoint {
    public static void main(String[] args) {
        SpringApplication.run(PocEntryPoint.class);
    }

    @Bean
    public CustomerService cstService(CustomerRepository repo) {
        return new CustomerService(repo);
    }

    @Bean
    public OrderService orderService(OrderRepository repo) {
        return new OrderService(repo);
    }
}
