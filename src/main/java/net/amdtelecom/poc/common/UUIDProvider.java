package net.amdtelecom.poc.common;

import java.util.UUID;

/**
 * XXX;
 * This class exists for the sole purpose of making UUID testable with Mockito.
 */
public class UUIDProvider {
    public UUID random() {
        return UUID.randomUUID();
    }
}
