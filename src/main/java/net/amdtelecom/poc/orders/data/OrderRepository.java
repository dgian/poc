package net.amdtelecom.poc.orders.data;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;

public interface OrderRepository extends MongoRepository<Order, String> {

    Collection<Order> findByCustomerId(String customerId);
}
