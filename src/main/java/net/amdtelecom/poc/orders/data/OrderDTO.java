package net.amdtelecom.poc.orders.data;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.UUID;

public class OrderDTO {
    String id;
    String customerId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime createdAt;

    String info;

    public static OrderDTO createEmpty() {
        final OrderDTO ord = new OrderDTO();
        ord.setId(UUID.randomUUID().toString());
        ord.setCreatedAt(LocalDateTime.now());

        return ord;
    }

    @Override
    public String toString() {
        return String.format("OrderDTO{id=%s, customerId=%s, createdAt=%s, info='%s'}", id, customerId, createdAt,
                info);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
