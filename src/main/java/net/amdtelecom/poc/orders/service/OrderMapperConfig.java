package net.amdtelecom.poc.orders.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import net.amdtelecom.poc.orders.data.Order;
import net.amdtelecom.poc.orders.data.OrderDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class OrderMapperConfig {
    @Bean
    MapperFacade facade() {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(Order.class, OrderDTO.class)
                .byDefault()
                .register();

        factory.getConverterFactory().registerConverter(
                new PassThroughConverter(LocalDateTime.class)
        );

        return factory.getMapperFacade();
    }
}
