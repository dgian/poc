package net.amdtelecom.poc.orders.service;

import net.amdtelecom.poc.common.ResourceNotFoundException;
import net.amdtelecom.poc.orders.data.Order;
import net.amdtelecom.poc.orders.data.OrderRepository;

import java.util.*;

public class OrderService {
    private final OrderRepository repo;

    public OrderService(OrderRepository repo) {
        this.repo = repo;
    }

    public Order create(Order ord) {
        return repo.insert(ord);
    }

    public Order read(String uuid) {
        Optional<Order> ord = repo.findById(uuid);
        return ord.orElseThrow(ResourceNotFoundException::new);
    }

    public Order update(Order ord) {
        return repo.save(ord);
    }

    public void delete(String id) {
        repo.deleteById(id);
    }

    public Collection<Order> list() {
        return repo.findAll();
    }

    public Collection<Order> filterByCustomer(String cId) {
        return repo.findByCustomerId(cId);
    }
}
