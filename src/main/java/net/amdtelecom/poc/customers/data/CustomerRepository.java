package net.amdtelecom.poc.customers.data;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public interface CustomerRepository extends MongoRepository<Customer, String> {

}
