package net.amdtelecom.poc.customers.data;

import java.util.UUID;

public class CustomerDTO {
    String id;
    String firstName;
    String lastName;
    double balance;

    public static CustomerDTO createEmpty() {
        final CustomerDTO cst = new CustomerDTO();
        cst.setId(UUID.randomUUID().toString());

        return cst;
    }

    @Override
    public String toString() {
        return String.format("CustomerDTO{id=%s, firstName='%s', lastName='%s', balance=%s}", id, firstName, lastName,
                balance);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
