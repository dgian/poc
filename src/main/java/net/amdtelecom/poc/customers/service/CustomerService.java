package net.amdtelecom.poc.customers.service;

import net.amdtelecom.poc.common.ResourceNotFoundException;
import net.amdtelecom.poc.customers.data.Customer;
import net.amdtelecom.poc.customers.data.CustomerRepository;

import java.util.Collection;
import java.util.Optional;

public class CustomerService {
    private final CustomerRepository repo;

    public CustomerService(CustomerRepository repo) {
        this.repo = repo;
    }

    public Customer create(Customer cst) {
        return repo.insert(cst);
    }

    public Customer read(String uuid) {
        Optional<Customer> cst = repo.findById(uuid);
        return cst.orElseThrow(ResourceNotFoundException::new);
    }

    public Customer update(Customer cst) {
        return repo.save(cst);
    }

    public Collection<Customer> list() {
        return repo.findAll();
    }
}
