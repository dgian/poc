package net.amdtelecom.poc.customers.service;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import net.amdtelecom.poc.customers.data.Customer;
import net.amdtelecom.poc.customers.data.CustomerDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class CustomerMapperConfig {
    @Bean
    MapperFacade facade() {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();
        factory.classMap(Customer.class, CustomerDTO.class)
                .byDefault()
                .register();

        factory.getConverterFactory().registerConverter(
                new PassThroughConverter(LocalDateTime.class)
        );

        return factory.getMapperFacade();
    }
}
