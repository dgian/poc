package net.amdtelecom.poc.api;

import ma.glasnost.orika.MapperFacade;
import net.amdtelecom.poc.customers.data.Customer;
import net.amdtelecom.poc.customers.data.CustomerDTO;
import net.amdtelecom.poc.customers.service.CustomerService;
import net.amdtelecom.poc.orders.data.Order;
import net.amdtelecom.poc.orders.data.OrderDTO;
import net.amdtelecom.poc.orders.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/customer",
                produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerResource {
    private final CustomerService cstService;
    private final OrderService ordService;
    private final MapperFacade facade;

    CustomerResource(CustomerService cstService, OrderService ordService, MapperFacade facade) {
        this.cstService = cstService;
        this.ordService = ordService;
        this.facade = facade;
    }

    @RequestMapping(method = RequestMethod.POST)
    CustomerDTO create(@RequestBody CustomerDTO cstDto) {
        cstDto.setId(UUID.randomUUID().toString());
        final Customer cst = facade.map(cstDto, Customer.class);
        cstService.create(cst);

        return cstDto;
    }

    @RequestMapping(path = "/{uuid}",
                    method = RequestMethod.GET)
    CustomerDTO read(@PathVariable String uuid) {
        final Customer cst = cstService.read(uuid);

        return facade.map(cst, CustomerDTO.class);
    }

    @RequestMapping(method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    CustomerDTO update(@RequestBody CustomerDTO cstDto) {
        final Customer cst = facade.map(cstDto, Customer.class);
        cstService.update(cst);

        return cstDto;
    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<CustomerDTO> list() {
        Collection<Customer> customers = cstService.list();

        return customers
                .stream()
                .map(cst -> facade.map(cst, CustomerDTO.class))
                .collect(Collectors.toList());
    }

    @RequestMapping(path = "/{uuid}/orders")
    Collection<OrderDTO> orders(@PathVariable String uuid) {
        final Collection<Order> orders = ordService.filterByCustomer(uuid);
        System.out.println("orders = " + orders);

        return orders
                .stream()
                .map(ord -> facade.map(ord, OrderDTO.class))
                .collect(Collectors.toList());
    }
}
