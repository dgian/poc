package net.amdtelecom.poc.api;

import ma.glasnost.orika.MapperFacade;
import net.amdtelecom.poc.orders.data.Order;
import net.amdtelecom.poc.orders.data.OrderDTO;
import net.amdtelecom.poc.orders.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/order",
                produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderResource {
    private final OrderService ordService;
    private final MapperFacade facade;

    public OrderResource(OrderService orderService, MapperFacade facade) {
        this.ordService = orderService;
        this.facade = facade;
    }

    @RequestMapping(method = RequestMethod.POST)
    OrderDTO create(@RequestBody OrderDTO ordDto) {
        ordDto.setId(UUID.randomUUID().toString());
        ordDto.setCreatedAt(LocalDateTime.now());
        final Order ord = facade.map(ordDto, Order.class);
        ordService.create(ord);

        return ordDto;
    }

    @RequestMapping(path = "/{uuid}",
                    method = RequestMethod.GET)
    OrderDTO read(@PathVariable final String uuid) {
        final Order ord = ordService.read(uuid);

        return facade.map(ord, OrderDTO.class);
    }

    @RequestMapping(method = RequestMethod.PUT,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    OrderDTO update(@RequestBody final OrderDTO ordDto) {
        final Order ord = facade.map(ordDto, Order.class);
        ordService.update(ord);

        return ordDto;
    }

    @RequestMapping(path = "/{uuid}",
                    method = RequestMethod.DELETE,
                    produces = MediaType.TEXT_PLAIN_VALUE)
    String delete(@PathVariable final String uuid) {
        ordService.delete(uuid);
        return String.format("Deleted order with id %s", uuid);
    }

    @RequestMapping(method = RequestMethod.GET)
    Collection<OrderDTO> list() {
        Collection<Order> orders = ordService.list();

        return orders
                .stream()
                .map(order -> facade.map(order, OrderDTO.class))
                .collect(Collectors.toList());
    }
}
