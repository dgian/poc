package net.amdtelecom.poc.orders.data;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import com.mongodb.MongoClient;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OrderRepositoryTest.Config.class)
@UsingDataSet
public class OrderRepositoryTest {
    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("my-test");

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    OrderRepository ordRepository;

    @Configuration
    @EnableMongoRepositories(basePackageClasses = OrderRepository.class)
    static class Config extends AbstractMongoConfiguration {
        @Override
        @Bean
        public MongoClient mongoClient() {
            return new MongoClient("localhost");
        }

        @Override
        protected String getDatabaseName() {
            return "my-test";
        }
    }

    @Test
    public void shouldCreateOrder() {
        Order ord = Order.createEmpty();
        ordRepository.save(ord);
        assertTrue(ordRepository.findById(ord.getId()).isPresent());
    }

    @Test
    public void shouldReadOrder() {
        Optional<Order> ord = ordRepository.findById("ddc1e32d-d314-426d-bd80-62cc0dba9647");
        assertTrue(ord.isPresent());
    }

    @Test
    public void shouldUpdateOrder() {
        Order ord = ordRepository.findById("ddc1e32d-d314-426d-bd80-62cc0dba9647").get();
        ord.setCustomerId("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5");
        ordRepository.save(ord);

        Order ordAfter = ordRepository.findById("ddc1e32d-d314-426d-bd80-62cc0dba9647").get();
        assertTrue(ordAfter.getCustomerId().equals("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5"));
    }

    @Test
    public void shouldListOrders() {
        assertEquals(ordRepository.findAll().size(), 1);
    }

    @Test
    public void shouldFindByCustomer() {
        List<Order> all = ordRepository.findAll();
        for (Order order : all) {
            System.out.println("order.getCustomerId() = " + order.getCustomerId());
        }
        assertEquals(ordRepository.findByCustomerId("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5").size(), 1);
    }
}
