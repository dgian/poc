package net.amdtelecom.poc.orders.service;

import net.amdtelecom.poc.customers.data.CustomerRepository;
import net.amdtelecom.poc.customers.service.CustomerService;
import net.amdtelecom.poc.orders.data.Order;
import net.amdtelecom.poc.orders.data.OrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OrderServiceTest.Config.class)
public class OrderServiceTest {
    @Configuration
    static class Config {
        @Bean
        CustomerService cstService() {
            return new CustomerService(cstRepository());
        }

        @Bean
        OrderService orderService() {
            return new OrderService(ordRepository());
        }

        @Bean
        CustomerRepository cstRepository() {
            return mock(CustomerRepository.class);
        }

        @Bean
        OrderRepository ordRepository() {
            return mock(OrderRepository.class);
        }
    }

    @Autowired
    OrderService ordService;

    @Autowired
    OrderRepository ordRepository;

    @Test
    public void shouldCreateOrder() {
        final Order ord = Order.createEmpty();
        ordService.create(ord);
        verify(ordRepository).insert(ord);
    }

    @Test
    public void shouldReadOrder() {
        final Order ord = Order.createEmpty();
        final String uuid = ord.getId();

        when(ordRepository.findById(ord.getId())).thenReturn(Optional.of(ord));
        ordService.read(String.valueOf(uuid));

        verify(ordRepository).findById(uuid);
    }

    @Test
    public void shouldUpdateOrder() {
        final Order ord = Order.createEmpty();
        ordService.update(ord);
        verify(ordRepository).save(ord);
    }

    @Test
    public void shouldDeleteOrder() {
        final Order ord = Order.createEmpty();
        final String uuid = ord.getId();

        ordService.delete(uuid);
        verify(ordRepository).deleteById(uuid);
    }

    @Test
    public void shouldListOrders() {
        final List<Order> expected = Collections.unmodifiableList(
                Arrays.asList(new Order(), new Order())
        );

        when(ordRepository.findAll()).thenReturn(expected);

        final Collection<Order> list = ordService.list();

        verify(ordRepository).findAll();
        assertEquals(list.size(), expected.size());
    }

    @Test
    public void shouldFilterByCustomer() {
        final String cstUuid = UUID.randomUUID().toString();

        final List<Order> expected = Collections.unmodifiableList(
                Arrays.asList(new Order(), new Order())
        );

        when(ordRepository.findByCustomerId(cstUuid)).thenReturn(expected);

        final Collection<Order> orders = ordService.filterByCustomer(cstUuid);
        assertEquals(orders.size(), expected.size());
    }
}