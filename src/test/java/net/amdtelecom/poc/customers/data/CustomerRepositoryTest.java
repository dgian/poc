package net.amdtelecom.poc.customers.data;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import com.mongodb.MongoClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CustomerRepositoryTest.Config.class)
@UsingDataSet
public class CustomerRepositoryTest {

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("my-test");

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    CustomerRepository cstRepository;

    @Configuration
    @EnableMongoRepositories(basePackageClasses = CustomerRepository.class)
    static class Config extends AbstractMongoConfiguration {
        @Override
        @Bean
        public MongoClient mongoClient() {
            return new MongoClient("localhost");
        }

        @Override
        protected String getDatabaseName() {
            return "my-test";
        }
    }

    @Test
    public void shouldCreateCustomer() {
        Customer cst = Customer.createEmpty();
        cstRepository.save(cst);
        assertTrue(cstRepository.findById(cst.getId()).isPresent());
    }

    @Test
    public void shouldReadCustomer() {
        Optional<Customer> cst = cstRepository.findById("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5");
        assertTrue(cst.isPresent());
    }

    @Test
    public void shouldUpdateCustomer() {
        Customer cst = cstRepository.findById("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5").get();
        cst.setFirstName("first-name");
        cstRepository.save(cst);

        Customer cstAfter = cstRepository.findById("3da01d4b-1533-4bf6-8e16-d5d5f43c2de5").get();
        assertTrue(cstAfter.firstName.equals("first-name"));
    }

    @Test
    public void shouldListCustomers() {
        assertEquals(cstRepository.findAll().size(), 1);
    }
}