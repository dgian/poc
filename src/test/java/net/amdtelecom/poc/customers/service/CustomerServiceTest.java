package net.amdtelecom.poc.customers.service;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import net.amdtelecom.poc.customers.data.Customer;
import net.amdtelecom.poc.customers.data.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {CustomerServiceTest.Config.class})
public class CustomerServiceTest {

    @Configuration
    static class Config {
        @Bean
        CustomerService cstService() {
            return new CustomerService(cstRepository());
        }

        @Bean
        CustomerRepository cstRepository() {
            return mock(CustomerRepository.class);
        }
    }

    @Autowired
    private CustomerService cstService;

    @Autowired
    private CustomerRepository cstRepository;

    @Test
    public void shouldCreateCustomer() {
        Customer cst = Customer.createEmpty();
        cstService.create(cst);
        verify(cstRepository).insert(cst);
    }

    @Test
    public void shouldReadCustomer() {
        final Customer cst = Customer.createEmpty();
        final String uuid = cst.getId();

        when(cstRepository.findById(cst.getId())).thenReturn(Optional.of(cst));
        cstService.read(String.valueOf(uuid));

        verify(cstRepository).findById(uuid);
    }

    @Test
    public void shouldUpdateCustomer() {
        final Customer cst = Customer.createEmpty();
        cstService.update(cst);
        verify(cstRepository).save(cst);
    }

    @Test
    public void shouldListCustomers() {
        final List<Customer> expected = Collections.unmodifiableList(
                Arrays.asList(new Customer(), new Customer())
        );

        when(cstRepository.findAll()).thenReturn(expected);

        final Collection<Customer> list = cstService.list();

        verify(cstRepository).findAll();
        assertEquals(list.size(), expected.size());
    }

}